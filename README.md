# VPNC viwth OpenSSH server

Debian 10 container with bash, OpenShift CLI, kubernetes CLI, VPNC and OpenSSH server

## Connect to VPNC

### Run container

Run container with
```
docker run -d --name vpnc-tunnel -p 2222:22 \
           -e SSH_KEY="`cat ~/.ssh/id_rsa.pub`" \
           -e VPNC_GATEWAY=_GATEWAY_IP_ \
           -e VPNC_ID=_VPNC_ID_ \
           -e VPNC_SECRET='_VPNC_SECRET_' \
           -e VPNC_USERNAME=_VPNC_USERNAME_ \
           -e VPNC_PASSWORD=_VPNC_PASSWORD_ \
           --privileged \
           registry.gitlab.com/gabriel.arellano/vpnc-openssh-os-k8s:master
```
or with a local copy of VPNC config (named default.conf in this case):

```
docker run -d --name vpnc-tunnel -p 2222:22 \
           -e SSH_KEY="`cat ~/.ssh/id_rsa.pub`" \
           -v $(pwd)/default.conf:/etc/vpnc/default.conf \
           --privileged \
           registry.gitlab.com/gabriel.arellano/vpnc-openssh-os-k8s:master
```

### Connect to container

Connect to container as non privileged user
```
ssh -p 2222 docker@localhost
```

Connect to container as root user
```
ssh -p 2222 root@localhost
```
