FROM debian:10.9 AS build

ARG OC_VERSION=4.6
ARG CURL_VERSION=7.64.0-4+deb10u2

WORKDIR /tools

# hadolint ignore=DL3003,DL3015,DL4006,SC2046
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install curl=$CURL_VERSION -y && \
    mkdir oc && \
    cd oc && \
    curl -sLo /tmp/oc.tar.gz https://mirror.openshift.com/pub/openshift-v$(echo $OC_VERSION | cut -d'.' -f 1)/clients/oc/$OC_VERSION/linux/oc.tar.gz && \
    tar xzvf /tmp/oc.tar.gz -C /tools/oc/

FROM debian:10.9

LABEL maintainer="gabrielarellano@gmail.com"

ENV VPNC_GATEWAY=**None** \
    VPNC_ID=**None** \
    VPNC_SECRET=**None** \
    VPNC_USERNAME=**None** \
    VPNC_PASSWORD=**None**

ARG VPNC_VERSION=0.5.3r550-3
ARG OPEN_SSH_VERSION=1:7.9p1-10+deb10u2
ARG SUDO_VERSION=1.8.27-1+deb10u3

# hadolint ignore=DL3009,DL3059
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install vpnc=$VPNC_VERSION openssh-server=$OPEN_SSH_VERSION sudo=$SUDO_VERSION -y --no-install-recommends && \
    DEBIAN_FRONTEND=noninteractive apt-get clean autoclean && \
    DEBIAN_FRONTEND=noninteractive apt-get autoremove --yes

# hadolint ignore=DL3059
RUN mkdir -p /var/run/sshd && \
    sed -i "s/UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config && \
    sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    touch /root/.Xauthority && \
    true

## Set a default user. Available via runtime flag `--user docker`
## Add user to 'staff' group, granting them write privileges to /usr/local/lib/R/site.library
## User should also have & own a home directory, but also be able to sudo
# hadolint ignore=DL3059
RUN useradd docker \
        && passwd -d docker \
        && mkdir /home/docker \
        && chown docker:docker /home/docker \
        && addgroup docker staff \
        && addgroup docker sudo \
        && true

COPY --from=build /tools/oc/* /usr/bin/
COPY set_vpnc_config.sh /set_vpnc_config.sh
COPY set_root_pw.sh /set_root_pw.sh
COPY run.sh /run.sh
RUN chmod +x /*.sh

EXPOSE 22
CMD ["/run.sh"]

