#!/bin/bash

set -e

if [[ $VPNC_GATEWAY != "**None**" ]]; then
 /set_vpnc_config.sh;
fi

/set_root_pw.sh

/usr/sbin/vpnc-connect default

exec /usr/sbin/sshd -D